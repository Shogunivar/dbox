#if 1
#include <SPI.h>
#include <PN532_SPI.h>
#include <PN532.h>
#include <NfcAdapter.h>
#include <Servo.h>

PN532_SPI pn532spi(SPI, 10);
NfcAdapter nfc = NfcAdapter(pn532spi);
#else

#include <Wire.h>
#include <PN532_I2C.h>
#include <PN532.h>
#include <NfcAdapter.h>
#include <Servo.h>

PN532_I2C pn532_i2c(Wire);
NfcAdapter nfc = NfcAdapter(pn532_i2c);
#endif

Servo myservo;
int pos = 0;

void setup(void) {
  Serial.begin(9600);
  Serial.println("NDEF Reader");
  nfc.begin();
  myservo.attach(7);
  myservo.write(0);
  Serial.println("THIS SHOULD ONLY HAPPEN ONCE!");
}

void loop(void) {
 // Serial.println("\nScan a NFC tag\n");
  int sensorValue = analogRead(2);
  Serial.println(sensorValue);
  if(sensorValue <= 30){
    for (pos = pos; pos >= 0; pos -= 1) { // goes from 180 degrees to 0 degrees
      myservo.write(pos);              // tell servo to go to position in variable 'pos'
      delay(15);                       // waits 15ms for the servo to reach the position
    }
  }

  
  if (nfc.tagPresent())
  {
    NfcTag tag = nfc.read();
//    Serial.println(tag.getTagType());
//    Serial.print("UID: ");Serial.println(tag.getUidString());

    if (tag.hasNdefMessage()) // every tag won't have a message
    {

      NdefMessage message = tag.getNdefMessage();
//      Serial.print("\nThis NFC Tag contains an NDEF Message with ");
//      Serial.print(message.getRecordCount());
//      Serial.print(" NDEF Record");
      if (message.getRecordCount() != 1) {
//        Serial.print("s");
      }
//      Serial.println(".");

      // cycle through the records, printing some info from each
      int recordCount = message.getRecordCount();
      for (int i = 0; i < recordCount; i++)
      {
//        Serial.print("\nNDEF Record ");Serial.println(i+1);
        NdefRecord record = message.getRecord(i);
        // NdefRecord record = message[i]; // alternate syntax

//        Serial.print("  TNF: ");Serial.println(record.getTnf());
//        Serial.print("  Type: ");Serial.println(record.getType()); // will be "" for TNF_EMPTY

        // The TNF and Type should be used to determine how your application processes the payload
        // There's no generic processing for the payload, it's returned as a byte[]
        int payloadLength = record.getPayloadLength();
        byte payload[payloadLength];
        record.getPayload(payload);

        // Print the Hex and Printable Characters
//        Serial.print("  Payload (HEX): ");
        PrintHexChar(payload, payloadLength);

        // Force the data into a String (might work depending on the content)
        // Real code should use smarter processing
        String payloadAsString = "";
        for (int c = 0; c < payloadLength; c++) {
          payloadAsString += (char)payload[c];
        }
//        Serial.print("  Payload (as String): ");
//        Serial.print(payloadAsString);
        if(payloadAsString == "123456789") { 
          for (pos = pos; pos <= 180; pos += 1) { // goes from 0 degrees to 180 degrees
            // in steps of 1 degree
            myservo.write(pos);              // tell servo to go to position in variable 'pos'
            delay(15);                       // waits 15ms for the servo to reach the position
          }
//          Serial.println("WHY AREN'T YOU WORKING?!?!!!!?");
        }
//        Serial.println("WHY AREN'T YOU WORKING?!?!!!!? part 2");
//        Serial.println(payloadAsString);
        
      }
    }
  }
  delay(3000);
}
